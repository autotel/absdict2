<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210510090930 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE conjugated_definition (id INT AUTO_INCREMENT NOT NULL, word_id INT NOT NULL, declination_id INT NOT NULL, content LONGTEXT DEFAULT NULL, INDEX IDX_4C78B26E357438D (word_id), INDEX IDX_4C78B269941A932 (declination_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE declination (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, find_expression VARCHAR(255) NOT NULL, replace_expression VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE word_definition (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, definition LONGTEXT NOT NULL, updates LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE conjugated_definition ADD CONSTRAINT FK_4C78B26E357438D FOREIGN KEY (word_id) REFERENCES word_definition (id)');
        $this->addSql('ALTER TABLE conjugated_definition ADD CONSTRAINT FK_4C78B269941A932 FOREIGN KEY (declination_id) REFERENCES declination (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE conjugated_definition DROP FOREIGN KEY FK_4C78B269941A932');
        $this->addSql('ALTER TABLE conjugated_definition DROP FOREIGN KEY FK_4C78B26E357438D');
        $this->addSql('DROP TABLE conjugated_definition');
        $this->addSql('DROP TABLE declination');
        $this->addSql('DROP TABLE word_definition');
    }
}
