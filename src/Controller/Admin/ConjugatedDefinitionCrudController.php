<?php

namespace App\Controller\Admin;

use App\Entity\ConjugatedDefinition;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ConjugatedDefinitionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ConjugatedDefinition::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
