<?php

namespace App\Controller\Admin;

use App\Entity\Declination;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class DeclinationCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Declination::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
