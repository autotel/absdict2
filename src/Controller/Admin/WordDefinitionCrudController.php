<?php

namespace App\Controller\Admin;

use App\Entity\WordDefinition;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class WordDefinitionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return WordDefinition::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
