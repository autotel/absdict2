<?php

namespace App\Entity;

use App\Repository\ConjugatedDefinitionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConjugatedDefinitionRepository::class)
 */
class ConjugatedDefinition
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=WordDefinition::class, inversedBy="declination")
     * @ORM\JoinColumn(nullable=false)
     */
    private $word;

    /**
     * @ORM\ManyToOne(targetEntity=Declination::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $declination;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWord(): ?WordDefinition
    {
        return $this->word;
    }

    public function setWord(?WordDefinition $word): self
    {
        $this->word = $word;

        return $this;
    }

    public function getDeclination(): ?Declination
    {
        return $this->declination;
    }

    public function setDeclination(?Declination $declination): self
    {
        $this->declination = $declination;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }
}
