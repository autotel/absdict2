<?php

namespace App\Entity;

use App\Repository\DeclinationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DeclinationRepository::class)
 */
class Declination
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $findExpression;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $replaceExpression;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFindExpression(): ?string
    {
        return $this->findExpression;
    }

    public function setFindExpression(string $findExpression): self
    {
        $this->findExpression = $findExpression;

        return $this;
    }

    public function getReplaceExpression(): ?string
    {
        return $this->replaceExpression;
    }

    public function setReplaceExpression(?string $replaceExpression): self
    {
        $this->replaceExpression = $replaceExpression;

        return $this;
    }
}
