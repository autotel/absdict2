<?php

namespace App\Entity;

use App\Repository\WordDefinitionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WordDefinitionRepository::class)
 */
class WordDefinition
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $definition;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $updates;

    /**
     * @ORM\OneToMany(targetEntity=ConjugatedDefinition::class, mappedBy="word", orphanRemoval=true)
     */
    private $declination;

    public function __construct()
    {
        $this->declination = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDefinition(): ?string
    {
        return $this->definition;
    }

    public function setDefinition(string $definition): self
    {
        $this->definition = $definition;

        return $this;
    }

    public function getUpdates(): ?string
    {
        return $this->updates;
    }

    public function setUpdates(?string $updates): self
    {
        $this->updates = $updates;

        return $this;
    }

    /**
     * @return Collection|ConjugatedDefinition[]
     */
    public function getDeclination(): Collection
    {
        return $this->declination;
    }

    public function addDeclination(ConjugatedDefinition $declination): self
    {
        if (!$this->declination->contains($declination)) {
            $this->declination[] = $declination;
            $declination->setWord($this);
        }

        return $this;
    }

    public function removeDeclination(ConjugatedDefinition $declination): self
    {
        if ($this->declination->removeElement($declination)) {
            // set the owning side to null (unless already changed)
            if ($declination->getWord() === $this) {
                $declination->setWord(null);
            }
        }

        return $this;
    }
}
