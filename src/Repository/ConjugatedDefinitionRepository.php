<?php

namespace App\Repository;

use App\Entity\ConjugatedDefinition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ConjugatedDefinition|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConjugatedDefinition|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConjugatedDefinition[]    findAll()
 * @method ConjugatedDefinition[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConjugatedDefinitionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConjugatedDefinition::class);
    }

    // /**
    //  * @return ConjugatedDefinition[] Returns an array of ConjugatedDefinition objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConjugatedDefinition
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
