<?php

namespace App\Repository;

use App\Entity\WordDefinition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WordDefinition|null find($id, $lockMode = null, $lockVersion = null)
 * @method WordDefinition|null findOneBy(array $criteria, array $orderBy = null)
 * @method WordDefinition[]    findAll()
 * @method WordDefinition[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WordDefinitionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WordDefinition::class);
    }

    // /**
    //  * @return WordDefinition[] Returns an array of WordDefinition objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WordDefinition
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
